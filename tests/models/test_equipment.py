from unittest import TestCase

from src.models.equipment import Equipment, EquipmentCategory
from src.models.weapon import Weapon, WeaponCategory
from src.models.armor import Armor, ArmorCategory

from src.models.cost import Cost, CostUnit

class TestEquipment(TestCase):
    def test_equipment(self):
        equipment = Equipment(category=EquipmentCategory.ARMOR, name="Shield", cost=Cost(quantity=10, unit=CostUnit.GOLD), weight=1, desc="desc")

        self.assertEqual(equipment.name, "Shield")
        self.assertEqual(equipment.category, EquipmentCategory.ARMOR)
        self.assertEqual(equipment.cost.unit, CostUnit.GOLD)
        self.assertEqual(equipment.cost.quantity, 10)
        self.assertEqual(equipment.weight, 1)
        self.assertEqual(equipment.desc, "desc")


class TestWeapon(TestCase):
    def test_weapon(self):
        weapon = Weapon(category=EquipmentCategory.WEAPON, name="Longsword", cost=Cost(quantity=15, 
            unit=CostUnit.GOLD), weight=3, desc="desc", weapon_category=WeaponCategory.MARTIAL_MELEE, weapon_range="Melee", 
            category_range="Melee", damage_dice="1d8", damage_type="slashing", properties=["Versatile (1d10)"])

        self.assertEqual(weapon.name, "Longsword")
        self.assertEqual(weapon.category, EquipmentCategory.WEAPON)
        self.assertEqual(weapon.cost.unit, CostUnit.GOLD)
        self.assertEqual(weapon.cost.quantity, 15)
        self.assertEqual(weapon.weight, 3)
        self.assertEqual(weapon.desc, "desc")
        self.assertEqual(weapon.weapon_category, WeaponCategory.MARTIAL_MELEE)
        self.assertEqual(weapon.weapon_range, "Melee")
        self.assertEqual(weapon.category_range, "Melee")
        self.assertEqual(weapon.damage_dice, "1d8")
        self.assertEqual(weapon.damage_type, "slashing")
        self.assertEqual(weapon.properties, ["Versatile (1d10)"])

class TestArmor(TestCase):
    def test_armor(self):
        armor = Armor(category=EquipmentCategory.ARMOR, name="Breastplate", cost=Cost(quantity=400, unit=CostUnit.GOLD), 
            armor_category=ArmorCategory.HEAVY, weight=20, desc="desc", armor_class=14, armor_type="Medium", stealth_disadvantage=False)

        self.assertEqual(armor.name, "Breastplate")
        self.assertEqual(armor.category, EquipmentCategory.ARMOR)
        self.assertEqual(armor.armor_category, ArmorCategory.HEAVY)
        self.assertEqual(armor.cost.unit, CostUnit.GOLD)
        self.assertEqual(armor.cost.quantity, 400)
        self.assertEqual(armor.weight, 20)
        self.assertEqual(armor.desc, "desc")
        self.assertEqual(armor.armor_class, 14)
        self.assertEqual(armor.stealth_disadvantage, False)